 
 	<div class="footer">
   		<div class="container">
   			<div class="copy">
		       <p>&copy; 2014 Template by <a href="http://w3layouts.com" target="_blank"> w3layouts</a></p>
		    </div>
		    <div class="social">	
		      <ul>	
			   <li class="facebook"><a href="#"><span> </span></a></li>
			   <li class="twitter"><a href="#"><span> </span></a></li>
			   <li class="rss"><a href="#"><span> </span></a></li>	
			   <li class="google"><a href="#"><span> </span></a></li>			
		     </ul>
			</div>
			<div class="clearfix"></div>
   		</div>
   	</div>
   	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
   	<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.js"></script>
<!-- or -->
<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
<script type="text/javascript">
	
	$('.grid').masonry({
  // options
  itemSelector: '.grid-item',
  columnWidth: 200
});
</script>
</body>
</html>

