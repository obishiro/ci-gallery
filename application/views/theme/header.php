<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Shutter Bootstarp Website Template | Home :: w3layouts</title>
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!--<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>-->
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo base_url();?>assets/css/grid.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</script>
<!----webfonts---->
<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,500,700,900' rel='stylesheet' type='text/css'>
<!----//webfonts---->
<!----//requred-js-files---->
<script src="<?php echo base_url();?>assets/js/easing.js"></script>
	<script type="text/javascript" 	src="<?php echo base_url();?>assets/js/jquery.smint.js"></script>
		<script type="text/javascript">
			$(document).ready( function() {
			    $('.subMenu').smint({
			    	'scrollSpeed' : 1000
			    });
			});
		</script>	
</head>
<body>
	 

	<nav class="subMenu navbar-custom navbar-scroll-top smint"  >
	        <div class="container">
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-main-collapse">
	                    <img src="<?php echo base_url();?>assets/images/nav-icon.png" title="drop-down-menu"> 
	                </button>
	            </div>
	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="navbar-collapse navbar-left navbar-main-collapse collapse" style="height: 1px;">
	                <ul class="nav navbar-nav">
	                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
	                    <li class="active">
	                        <a id="sTop" class="subNavBtn active" href="#">Home</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="s1" class="subNavBtn" href="#">Portfolio</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="s2" class="subNavBtn" href="#">Services</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="s3" class="subNavBtn" href="#">Sign Up</a>
	                    </li>
	                   <li class="page-scroll">
	                        <a id="s4" class="subNavBtn" href="#">About</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="s5" class="subNavBtn" href="#">Contact</a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	             	<a id="s4" class="right-msg subNavBtn msg-icon" href="#"><span> </span></a>
	                <div class="clearfix"> </div>
	        </div>
	        <!-- /.container -->
   	  </nav>
   	  <div style="height:80px;"></div>
   	  <div class="portfolio s1" id="portfolio">
	  <div class="portfolio_box">